import simpleStore from "react-native-simple-store";

export const USER_KEY = "USER_KEY";

export default {
    save: (key, data) => {
        simpleStore
            .save(key, data)
            .catch(error => {
                console.error(error.message);
            });
    },
    get: (key) => {
        simpleStore.get(key)
            .then(user => {
                if (user) {
                    return simpleStore.delete(USER_KEY);
                }
            });
    },
}