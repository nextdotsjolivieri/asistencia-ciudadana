import {REGISTRATION_FAILURE, REGISTRATION_REQUEST, REGISTRATION_SUCCESS} from "./actions";
import env from "../config/env";

export const registrationRequest = () => ({type: REGISTRATION_REQUEST});
export const registrationSuccess = (data) => ({type: REGISTRATION_SUCCESS, data});
export const registrationFailure = (error) => ({type: REGISTRATION_FAILURE, error});

export function registration() {
    return dispatch => {
        dispatch(registrationRequest());
        fetch(env.api.url + "registration")
            .then(response => response.json())
            .then(responseJson => {
                if (responseJson.code === 400) {
                    dispatch(registrationFailure(responseJson.message));
                } else {
                    dispatch(registrationSuccess(responseJson));
                }
            })
            .catch(error => dispatch(registrationFailure(error)));
    }
}

const INITIAL_STATE = {
    registration: {loading: false, success: false, error: '', data: ''},
};

const baseReducer = (state = INITIAL_STATE, action) => {
    let error;
    let data;
    switch (action.type) {
        case REGISTRATION_REQUEST:
            return Object.assign({}, state, {
                registration: {loading: true, success: false, error: '', data: []}
            });

        case REGISTRATION_SUCCESS:
            data = action.data;
            return Object.assign({}, state, {
                registration: {loading: false, success: true, error: '', data}
            });

        case REGISTRATION_FAILURE:
            error = action.error;
            return Object.assign({}, state, {
                registration: {loading: false, success: false, error, data: []}
            });

        default:
            return state;
    }
};

export default baseReducer;