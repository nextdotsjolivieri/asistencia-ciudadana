import React, {Component} from "react";
import {PermissionsAndroid, Platform, StyleSheet, View} from "react-native";
import {Button, Text} from "native-base";
import {AudioRecorder, AudioUtils} from "react-native-audio";
import Sound from "react-native-sound";
import PushNotification from "react-native-push-notification";
import Camera from "react-native-camera";

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: '#fff',
        flex: 1,
        flexDirection: 'column',
        margin: 5,
        justifyContent: 'center'
    },
    capture: {
        // flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        color: '#000',
        padding: 10,
        margin: 40
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
});

class TestLibrariesC extends Component {

    constructor(props) {
        super(props);

        this.state = {
            currentTime: 0.0,
            recording: false,
            stoppedRecording: false,
            finished: false,
            audioPath: AudioUtils.DocumentDirectoryPath + '/test.aac',
            hasPermission: undefined,
        };
    }

    componentDidMount() {
        this._checkPermission().then((hasPermission) => {
            this.setState({hasPermission});

            if (!hasPermission) return;

            this.prepareRecordingPath(this.state.audioPath);

            AudioRecorder.onProgress = (data) => {
                this.setState({currentTime: Math.floor(data.currentTime)});
            };

            AudioRecorder.onFinished = (data) => {
                // Android callback comes in the form of a promise instead.
                if (Platform.OS === 'ios') {
                    this._finishRecording(data.status === "OK", data.audioFileURL);
                }
            };
        });

        PushNotification.localNotification({
            title: "Notification", // (required)
            message: "My Notification Message", // (required)
        });
    }

    prepareRecordingPath = (audioPath) => {
        AudioRecorder.prepareRecordingAtPath(audioPath, {
            SampleRate: 22050,
            Channels: 1,
            AudioQuality: "Low",
            AudioEncoding: "mp3",
            AudioEncodingBitRate: 32000
        });
    };

    _checkPermission() {
        if (Platform.OS !== 'android') {
            return Promise.resolve(true);
        }

        const rationale = {
            'title': 'Microphone Permission',
            'message': 'AudioExample needs access to your microphone so you can record audio.'
        };

        return PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.RECORD_AUDIO, rationale)
            .then((result) => {
                console.log('Permission result:', result);
                return (result === true || result === PermissionsAndroid.RESULTS.GRANTED);
            });
    }

    _finishRecording = (didSucceed, filePath) => {
        this.setState({finished: didSucceed});
        console.log(`Finished recording of duration ${this.state.currentTime} seconds at path: ${filePath}`);
    };

    _renderButton(title, onPress, active) {
        const style = (active) ? styles.activeButtonText : styles.buttonText;

        return (
            <Button style={styles.button} onPress={onPress}>
                <Text style={style}>
                    {title}
                </Text>
            </Button>
        );
    }

    async _record() {
        if (this.state.recording) {
            console.warn('Already recording!');
            return;
        }

        if (!this.state.hasPermission) {
            console.warn('Can\'t record, no permission granted!');
            return;
        }

        if (this.state.stoppedRecording) {
            this.prepareRecordingPath(this.state.audioPath);
        }

        this.setState({recording: true});

        try {
            const filePath = await AudioRecorder.startRecording();
        } catch (error) {
            console.error(error);
        }
    };

    async _play() {
        if (this.state.recording) {
            await this._stop();
        }

        // These timeouts are a hacky workaround for some issues with react-native-sound.
        // See https://github.com/zmxv/react-native-sound/issues/89.
        setTimeout(() => {
            const sound = new Sound(this.state.audioPath, '', (error) => {
                if (error) {
                    console.log('failed to load the sound', error);
                }
            });

            setTimeout(() => {
                sound.play((success) => {
                    if (success) {
                        console.log('successfully finished playing');
                    } else {
                        console.log('playback failed due to audio decoding errors');
                    }
                });
            }, 100);
        }, 100);
    };

    async _stop() {
        if (!this.state.recording) {
            console.warn('Can\'t stop, not recording!');
            return;
        }

        this.setState({stoppedRecording: true, recording: false});

        try {
            const filePath = await AudioRecorder.stopRecording();

            if (Platform.OS === 'android') {
                this._finishRecording(true, filePath);
            }
            return filePath;
        } catch (error) {
            console.error(error);
        }
    };

    async _pause() {
        if (!this.state.recording) {
            console.warn('Can\'t pause, not recording!');
            return;
        }

        this.setState({stoppedRecording: true, recording: false});

        try {
            const filePath = await AudioRecorder.pauseRecording();

            // Pause is currently equivalent to stop on Android.
            if (Platform.OS === 'android') {
                this._finishRecording(true, filePath);
            }
        } catch (error) {
            console.error(error);
        }
    };

    takePicture = () => {

        //  Configuration to Image capture
        // const options = {};
        // // Camera.constants.CaptureMode.video
        // //options.location = ...
        // this.camera.capture({metadata: options})
        //     .then((data) => console.log(data))
        //     .catch(err => console.error(err));

        //// Configuration to Video capture
        this.camera.capture({mode: Camera.constants.CaptureMode.video})
            .then((data) => console.log(data))
            .catch((err) => console.log(err))
    };

    render() {
        return (
            <View style={styles.container}>
                {this._renderButton("RECORD", () => {
                    this._record()
                }, this.state.recording)}
                {this._renderButton("PLAY", () => {
                    this._play()
                })}
                {this._renderButton("STOP", () => {
                    this._stop()
                })}
                {this._renderButton("PAUSE", () => {
                    this._pause()
                })}
                <Text>{this.state.currentTime}s</Text>
                <Camera
                    ref={(cam) => {
                        this.camera = cam;
                    }}
                    style={styles.preview}
                    aspect={Camera.constants.Aspect.fill}>
                    <Text style={styles.capture} onPress={this.takePicture}>[CAPTURE]</Text>
                </Camera>
            </View>
        )
    }
}

export default TestLibrariesC;