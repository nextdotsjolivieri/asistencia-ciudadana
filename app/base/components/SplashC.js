import React, {Component} from "react";
import {Image, StyleSheet, View} from "react-native";
import {Actions} from "react-native-router-flux";

export default class SplashC extends Component {
    
    componentDidMount = () => {
        setTimeout(() => {
            Actions.main();
        }, 1500);
    };

    render() {
        return (
            <Image source={require('../../assets/fondo.png')} style={styles.container}/>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: null,
        height: null,
        resizeMode: 'stretch',

    }
});