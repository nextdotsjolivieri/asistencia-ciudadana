import React from "react";
import SplashC from "./components/SplashC";
import MainC from "./components/MainC";
import TestLibrariesC from "./components/TestLibrariesC";
import {ActionConst, Actions, Scene} from "react-native-router-flux";

const scenes = Actions.create(
    <Scene key="root">
        <Scene
            key="splash"
            component={SplashC}
            hideNavBar={true}
             initial={true}
            type={ActionConst.REPLACE}
        />
        <Scene
            key="main"
            component={MainC}
            hideNavBar={true}
            initial={false}
            type={ActionConst.REPLACE}
        />
        <Scene
            key="test"
            component={TestLibrariesC}
            hideNavBar={true}
            initial={false}
            type={ActionConst.REPLACE}
        />
    </Scene>
);

export default scenes;