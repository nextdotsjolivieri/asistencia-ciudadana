import React from "react";
import {AppRegistry} from "react-native";
import Root from "./app/base/containers/Root";

AppRegistry.registerComponent('asistenciaAlCiudadano', () => Root);

